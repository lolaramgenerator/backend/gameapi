from pydantic import BaseModel
from typing import List, Optional


class Game(BaseModel):
	name: Optional[str] = None
	password: Optional[str] = None
	closed: Optional[bool] = False
	rolls: Optional[int] = 0
	players: Optional[list] = []
	teams: Optional[list] = []
	availableHeroes: Optional[list] = []



