from services import *

from entGamePlayer import *
from dalGame import *
from const import *

import random




def caseLoadGames():
	return load()

def caseLoadGameById(idGame: int):
	return loadById(idGame)


def caseLoadGamePlayersById(idGame:int):
	gamePlayers = loadByIdGame(idGame)
	ret = []

	for gp in gamePlayers:
		player = loadPlayerFromService(gp['player'])
		ret.append(player)

	return ret


def caseAddPlayer(gamePlayer: GamePlayer):
	return addPlayer(gamePlayer)



def caseCreateGame(game: Game):
	return create(game)



def caseCreateTeams(idGame: int):

	rollHeroesByIdGame(idGame)
	rollTeamsByIdGame(idGame)

	

def rollTeamsByIdGame(idGame: int):
	players = loadByIdGame(idGame)
	game = loadById(idGame)
	side = sideA

	while len(players) > 0:

		total = len(players)
		rand = random.randint(0, total-1)
		side = sideA if side != sideA else sideB

		updateGamePlayer(idGame, players[rand]['player'], side)
		del players[rand]



def rollHeroesByIdGame(idGame: int):
	players = loadByIdGame(idGame)
	game = loadById(idGame)
	heroes = loadHeroesFromService()
	side = sideA
	countHeroes = len(heroes)
	totalheroesRolled = len(players) * game['rolls']
	
	deleteHeroRolled(idGame)


	while len(heroes) > (countHeroes - totalheroesRolled):

		total = len(heroes)
		rand = random.randint(1, total-1)
		side = sideA if side != sideA else sideB

		addHeroRolled(idGame, side, heroes[rand]['id'])
		del heroes[rand]




def caseHeroesRolled(idGame: int, side: str):
	heroesRolled = getHeroesRolled(idGame, side)
	ret = []

	for hr in heroesRolled:
		hero = loadHeroFromService(hr['hero'])
		hero['side'] = hr['side']

		ret.append(hero)
	return ret


def caseLoadGamePlayersByIdSide(idGame:int, side: str):
	
	gamePlayers = loadByIdGameSide(idGame, side)
	ret = []

	for gp in gamePlayers:
		player = loadPlayerFromService(gp['player'])
		ret.append(player)

	return ret



def caseUpdateGame(game: Game):
	return update(game)





#print(getHeroesRolled(1))


#caseCreateTeams(3)	
#print(caseLoadFullPlayersById(3))
#print(caseLoadGamePlayersByIdSide(3, 1))