
from peewee import *
from modelBase import *


class Game(BaseModel):
	name = CharField()
	password = CharField()
	rolls = IntegerField()
	closed = BooleanField()


class GamePlayer(BaseModel):
	game = ForeignKeyField(Game, backref='GamePlayers')
	player = IntegerField()
	side = CharField()
	#hero = ForeignKeyField(Hero, backref='GamePlayers')
	#rolls = IntegerField()
	
	class Meta:
		primary_key = CompositeKey('game', 'player')


class GameHeroRolled(BaseModel):
	game = ForeignKeyField(Game, backref='GamePlayers')
	side = CharField()
	hero = IntegerField()
	
	class Meta:
		primary_key = CompositeKey('game', 'side', 'hero')
