from pydantic import BaseModel
from typing import List, Optional


class GamePlayer(BaseModel):
	idGame: Optional[int] = 0
	idPlayer: Optional[int] = 0
	side: Optional[str] = None


