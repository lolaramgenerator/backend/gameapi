from peewee import *
from entGamePlayer import *
from modelGame import *
from util import *
from const import *


#GAME

def load():
	return ToList(Game.select().dicts().order_by(Game.closed, Game.id.desc()))

def loadById(idGame: int):
	return Game.select().where(Game.id == idGame).dicts().get()

def create(game: Game):
	Game.create(name=game.name, password=game.password, rolls=game.rolls, closed=game.closed)


#GAME PLAYER

def loadByIdGame(idGame: int):
	return ToList(GamePlayer.select().where(GamePlayer.game == idGame).dicts())


def loadByIdGameSide(idGame: int, side: str):
	return ToList(GamePlayer.select().where(GamePlayer.game == idGame, GamePlayer.side == side).dicts())


def updateGamePlayer(idGame: int, idPlayer: int, idSide: int):
	GamePlayer.update(side = idSide).where(GamePlayer.game == idGame, GamePlayer.player == idPlayer).execute()


#GAME HERO ROLLED

def deleteHeroRolled(idGame: int):
	GameHeroRolled.delete().where(GameHeroRolled.game == idGame).execute()


def addHeroRolled(idGame: int, side: str, idHero: int):
	GameHeroRolled.create(game = idGame, side= side, hero = idHero)


def getHeroesRolled(idGame: int, side: str):
	return ToList(GameHeroRolled.select().where(GameHeroRolled.game == idGame, GameHeroRolled.side == side).dicts())







def update(game: Game):
	Game.update(name=game.name, password=game.password, rolls=game.rolls, closed=game.closed).where( Game.id == game.id).execute()

def deleteByIdGame(idGame: int):
	Game.delete().where(Game.id == idGame).execute()



def addPlayer(gamePlayer: GamePlayer):
	print(gamePlayer)
	GamePlayer.create(game=gamePlayer.idGame, player=gamePlayer.idPlayer, side=gamePlayer.side)







def getPrratoda():
	return ToList(GameHeroRolled.select().dicts())


#GamePlayer.delete().where(GamePlayer.game==3).execute()

#print(getHeroesRolled(3))
#create(1,1)
#create(1,2)


# idGame = 2

# addPlayer(idGame,1,0)
# addPlayer(idGame,2,0)
# addPlayer(idGame,4,0)
# addPlayer(idGame,5,0)
# addPlayer(idGame,6,0)
# addPlayer(idGame,7,0)


#1,4,5,6,7,9
#updateTeamPlayer(1,1,2,0)

#print(loadByIdGame(1))

#print(load())
#print(loadByIdGame(3))

