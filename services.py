import requests
from const import *





def loadPlayerFromService(idPlayer:int):
	resp = requests.get(url= linkPlayerAPI + '/Players/' + str(idPlayer))
	data = resp.json()
	return data




def loadHeroesFromService():
	resp = requests.get(url= linkHeroAPI + '/Heroes/')
	data = resp.json()
	return data



def loadHeroFromService(idHero:int):
	resp = requests.get(url= linkHeroAPI + '/Heroes/' + str(idHero))
	data = resp.json()
	return data

