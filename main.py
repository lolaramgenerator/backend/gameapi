from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from caseGame import *
from entGamePlayer import *
from entGame import *
from entGameRet import *


app = FastAPI()


origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/Games/")
async def getGames():
    return caseLoadGames()

@app.get("/Games/{idGame}")
async def getGames(idGame: int):
    return caseLoadGameById(idGame)

@app.get("/Games/{idGame}/Players")
async def getGames(idGame: int):
    return caseLoadGamePlayersById(idGame)


@app.post("/Games/")
async def createGame(game: Game):
    return caseCreateGame(game)

@app.post("/Games/{idGame}/Teams/")
async def createTeams(idGame: int):
    return caseCreateTeams(idGame)


@app.get("/Games/{idGame}/Heroes/Side/{side}")
async def getHeroesRolled(idGame: int, side: str):
    return caseHeroesRolled(idGame, side)


@app.get("/Games/{idGame}/Players/Side/{idSide}")
async def getTeams(idGame: int, idSide: str):
    return caseLoadGamePlayersByIdSide(idGame, idSide)    


@app.post("/Games/{idGame}/Players/")
async def addGamePlayer(gamePlayer: GamePlayer):
    return caseAddPlayer(gamePlayer)