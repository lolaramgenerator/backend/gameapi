from peewee import *




db = SqliteDatabase('GameDB.db')

linkChampions = 'https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champion-summary.json'
linkIconsBase ='https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champion-icons/'
linkChampionsBase = 'https://raw.communitydragon.org/latest/plugins/rcp-be-lol-game-data/global/default/v1/champions/'

linkPlayerAPI = 'http://127.0.0.1:8001'
linkHeroAPI = 'http://127.0.0.1:8002'

sideA = 'Blue'
sideB = 'Red'