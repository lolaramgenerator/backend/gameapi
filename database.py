from peewee import *
from const import db
from modelGame import *




def create_tables():
    with db:
        db.create_tables([Game,GamePlayer,GameHeroRolled])

create_tables()


